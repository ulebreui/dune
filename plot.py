import matplotlib.pyplot as plt
import numpy as np
import glob
import dune
import os


from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from matplotlib.colors import ListedColormap
from matplotlib.ticker import MaxNLocator
#Plot var2 against var1

def plot_slice(sharkdata,var1,var2,var1_array,var2_array,label_array,multiple_var=False,path="",figname="",save=False,scale_x='log',scale_y='log',show=True,title='time',label=None,update=None,xlab=None,ylab=None,color=None,linestyle=None,axes=None,marker=None,markerfacecolor=None):
    if axes:
        theAxes = axes
    else:
        #plt.subplot(111)
        plt.figure()
        theAxes = plt.gca()
    if(update):
        sharkdata= dune.SharkData(update)
    if(xlab==None):
        xlab=getattr(sharkdata,var1+'_label')
    if(ylab==None):
        ylab=getattr(sharkdata,var2+'_label')


    if(multiple_var==False):
    
        theAxes.plot(getattr(sharkdata,var1),getattr(sharkdata,var2),label=label,color=color,linestyle=linestyle,marker=marker,markerfacecolor=markerfacecolor)

    if (multiple_var):
        for i in range(0,len(var1_array)):
            theAxes.plot(getattr(sharkdata,var1_array[i]),getattr(sharkdata,var2_array[i]),label=label_array[i],color=color,linestyle=linestyle,marker=marker,markerfacecolor=markerfacecolor)

    if(title=='time'):
        theAxes.set_title(str(round(float(getattr(sharkdata,"time")),2))+" kyr")
        
    theAxes.set_title(title)

    theAxes.set_xlabel(xlab)
    theAxes.set_ylabel(ylab)
    theAxes.set_xscale(scale_x)
    theAxes.set_yscale(scale_y)

    plt.legend()
    if(show):
        plt.show()

    if(save):
        os.chdir(path)
        plt.savefig(figname+".png")
    return sharkdata

#Plot the dust distribution at a given radius
def plot_distribution(sharkdata,radius=1,ir=None,show=True,title='time',label=None,update=None,ylim=None,color=None,linestyle=None,axes=None,marker=None,markerfacecolor=None,cumulative=False,normalise=False,unit_x=None,number=None):
    if axes:
        theAxes = axes
    else:
        plt.subplot(111)
        theAxes = plt.gca()
        
    if(update):
        sharkdata= dune.SharkData(update)
    if(ir):
        index=ir
    else:
        rgrid=abs(getattr(sharkdata,"grid")-radius)
        indexi=np.argwhere(rgrid==np.amin(rgrid))
        index=indexi[0][0]
        print("plotting distribution at r="+str(rgrid[index]+radius),"index", index)
        
    if(title=='time'):
        theAxes.set_title(str(round(float(getattr(sharkdata,"time")),1))+" kyr")
        if(getattr(sharkdata,"time")==0.):
            theAxes.set_title("0 kyr")
        

    if(ylim):
        theAxes.set_ylim(ylim[0],ylim[1])
    theAxes.set_xscale("log")
    theAxes.set_yscale("log")
    theAxes.set_xlabel(getattr(sharkdata,"sdust_label"))
    theAxes.set_ylabel(getattr(sharkdata,"epsilon_label"))
    sdust=getattr(sharkdata,"sdust_all")
    sconv=1
    if(unit_x=='micron'):
        sconv=1e4
        theAxes.set_xlabel('s [$\\mu$m]')

    epsdust=getattr(sharkdata,"epsilon_all")
    eps=epsdust[index,:]
    if(cumulative):
        eps=np.sum(eps)-np.cumsum(eps)
        theAxes.set_yscale("linear")

    if(number=='number'):
        nH=getattr(sharkdata,"nh")
        rho=getattr(sharkdata,"rho")
        eps=rho[index]*epsdust[index,:]/np.sqrt(getattr(sharkdata,"mplus")*getattr(sharkdata,"mminus"))/nH[index]
        theAxes.set_ylabel("$N_{\mathrm{dust}}/N_{\mathrm{H}}$")
    if(normalise):
        splus=getattr(sharkdata,"aplus")
        sminus=getattr(sharkdata,"aminus")
        area=np.sum(eps*(splus-sminus))
        eps/=area

        theAxes.set_ylabel("PDF($\\epsilon$)")
        if(number=='number'):
            theAxes.set_ylabel("PDF($N_{\mathrm{dust}}/N_{\mathrm{H}}$)")
    theAxes.plot(sdust[index,:]*sconv,eps,label=label,color=color,linestyle=linestyle,marker=marker,markerfacecolor=markerfacecolor)

    if(show):
        if(label):
            plt.legend()
        plt.show()
    return sharkdata

#Plot the dust distribution at a given radius for the tests
def plot_distribution_test(sharkdata,show=True,title='time',label=None,update=None,ylim=None,color=None,linestyle='solid',marker=None):
    if(update):
        sharkdata= dune.SharkData(update)

    if(title=='time'):
        plt.title(str(round(float(getattr(sharkdata,"time")),2))+" kyr")

    if(ylim):
        plt.ylim(ylim[0],ylim[1])
    sdust=getattr(sharkdata,"sdust_all")
    epsdust=getattr(sharkdata,"epsilon_all")
    
    plt.loglog(sdust**3,epsdust,label=label,color=color,linestyle=linestyle,marker=marker)
    plt.xlabel("m")
    plt.ylabel("$\\rho_{\\mathrm{d},m}$")
    if(show):
        if(label):
            plt.legend()
        plt.show()
    return sharkdata
def plot_distribution_1cell(sharkdata,show=True,title='time',label=None,update=None,ylim=None,color=None,linestyle='solid',marker=None):
    if(update):
        sharkdata= dune.SharkData(update)

    if(title=='time'):
        plt.title(str(round(float(getattr(sharkdata,"time")),2))+" kyr")

    if(ylim):
        plt.ylim(ylim[0],ylim[1])
    sdust=getattr(sharkdata,"sdust_all")
    epsdust=getattr(sharkdata,"epsilon_all")
    
    plt.loglog(sdust,epsdust,label=label,color=color,linestyle=linestyle,marker=marker)
    plt.xlabel(getattr(sharkdata,"sdust_label"))
    plt.ylabel(getattr(sharkdata,"epsilon_label"))
    if(show):
        if(label):
            plt.legend()
        plt.show()
    return sharkdata


def plot_2d_distribution(sharkdata,varx,scale_x='log',scale_y='log',scale_z='log',show=True,title='time',label=None,update=None,cmap='viridis'):
    if(update):
        sharkdata= dune.SharkData(update)
    y=getattr(sharkdata,varx)
    xx=getattr(sharkdata,"sdust_all")
    x=xx[0,:]
    z=np.log10(getattr(sharkdata,"epsilon_all"))
    levels=np.linspace(np.amin(z),np.amax(z),256)
    cmap = plt.get_cmap(cmap)
    norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    cs=plt.pcolormesh(x,y,z,norm=norm, cmap=cmap)
    cm=plt.colorbar(cs,label='log of '+getattr(sharkdata,"epsilon_label"))
    plt.xscale(scale_x)
    plt.yscale(scale_y)
    plt.ylabel(getattr(sharkdata,varx+"_label"))
    plt.xlabel(getattr(sharkdata,'sdust_label'))
    plt.title('Dust distribution')
    if(show):
        if(label):
            plt.legend()
        plt.show()               

        

def plot_drift(sharkdata,varx,radius=1,ir=None,scale_x='log',scale_y='log',scale_z='log',show=True,title='time',label=None,update=None,cmap='viridis'):
    if(update):
        sharkdata= dune.SharkData(update)
    if(ir):
        index=ir
    else:
        rgrid=abs(getattr(sharkdata,"grid")-radius)
        indexi=np.argwhere(rgrid==np.amin(rgrid))
        index=indexi[0][0]
        print("plotting velocities at r="+str(rgrid[index]+radius),"index", index)
    xx=getattr(sharkdata,"sdust_all")
    x=xx[0,:]
    zz=(abs(getattr(sharkdata,varx)))
    zzz=zz[index,:,:]
    z=np.log10(abs(zzz))
    try:
        levels=np.linspace(np.amin(np.log10(zzz[zzz>0])),np.amax(z),256)
        cmap = plt.get_cmap(cmap)
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
        cs=plt.pcolormesh(x,x,z,norm=norm, cmap=cmap)
        cm=plt.colorbar(cs,label='Drift velocity [cm/s]')
    except:
        print('toto')
    plt.xscale(scale_x)
    plt.yscale(scale_y)
    plt.xlabel(getattr(sharkdata,'sdust_label'))
    plt.ylabel(getattr(sharkdata,'sdust_label'))

    plt.title('Drift velocitty')
    if(show):
        if(label):
            plt.legend()
        plt.show()               
        
def plot_domin_drift(sharkdata,radius=1,ir=None,scale_x='log',scale_y='log',scale_z='log',show=True,title='time',label=None,update=None,cmap='viridis'):
    if(update):
        sharkdata= dune.SharkData(update)
    if(ir):
        index=ir
    else:
        rgrid=abs(getattr(sharkdata,"grid")-radius)
        indexi=np.argwhere(rgrid==np.amin(rgrid))
        index=indexi[0][0]
        print("plotting dominant velocities at r="+str(rgrid[index]+radius),"index", index)
    xx=getattr(sharkdata,"sdust_all")
    x=xx[0,:]
    zz=(abs(getattr(sharkdata,"vdrift_hydro")))
    hydro_drift=zz[index,:,:]
    zz=(abs(getattr(sharkdata,"vdrift_ad")))
    ad_drift=zz[index,:,:]
    zz=(abs(getattr(sharkdata,"vdrift_brow")))
    brow_drift=zz[index,:,:]
    zz=(abs(getattr(sharkdata,"vdrift_turb")))
    turb_drift=zz[index,:,:]
    max_drift=np.maximum(ad_drift,np.maximum(hydro_drift,np.maximum(brow_drift,turb_drift)))

    XXX=turb_drift*0.0
    XXX=np.where(turb_drift==max_drift,1,XXX)
    XXX=np.where(hydro_drift==max_drift,2,XXX)
    XXX=np.where(ad_drift==max_drift,3,XXX)
    XXX=np.where(brow_drift==max_drift,4,XXX)
    cmap = ListedColormap(['brown','seagreen','turquoise','purple'])  # color map for [0, 2]
    levels = MaxNLocator(nbins=4).tick_values(1, 4)
    norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    cs=plt.pcolormesh(x, x, XXX, cmap=cmap,norm=norm)
    cbar = plt.colorbar()
    cbar.set_ticks([1,2,3,4])
    cbar.set_ticklabels(['Turbulence', 'Hydro', 'Ambipolar','Brownian'])
    plt.xscale(scale_x)
    plt.yscale(scale_y)
    plt.xlabel(getattr(sharkdata,'sdust_label'))
    plt.ylabel(getattr(sharkdata,'sdust_label'))

    plt.title('Dominant collision velocity')
    if(show):
        if(label):
            plt.legend()
        plt.show()               

