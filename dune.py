import matplotlib.pyplot as plt
import numpy as np
import glob
import os

# DUNE is a free software : you can redistribute it/and or modify it
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#You should have received a copy of the GNU General Public License
#along with DUNE.  If not, see <http://www.gnu.org/licenses/>.



class SharkData():
    def __init__(self,nout=1,path="",read_drift=False):

        print("He who controls the spice, controls the universe")
        print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
        print("|||      |||||||  ||||  ||||     |||||  ||||         ||||")
        print("||| |||   ||||||  ||||  ||||  ||  ||||  ||||  |||||||||||")
        print("||| ||||   |||||  ||||  ||||  ||||  ||  ||||  |||||||||||")
        print("||| |||||   ||||  ||||  ||||  |||||  |  ||||         ||||")
        print("||| |||||   ||||  ||||  ||||  ||||||    ||||  |||||||||||")
        print("||| ||||   |||||  ||||  ||||  |||||||   ||||  |||||||||||")
        print("|||     ||||||||        ||||  ||||||||  ||||         ||||")
        print("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
        filename=self.data_loader(nout=nout,path=path)
        info = np.loadtxt(filename+"/info.dat")
        grid = np.loadtxt(filename+"/grid.dat")
        rho  = np.loadtxt(filename+"/rho.dat")
        v  = np.loadtxt(filename+"/v.dat")
        B  = np.loadtxt(filename+"/B.dat")
        if (os.path.exists(filename+"/By.dat")):
            By  = np.loadtxt(filename+"/By.dat")
            Bz  = np.loadtxt(filename+"/Bz.dat")
            vy  = np.loadtxt(filename+"/vy.dat")
            vz  = np.loadtxt(filename+"/vz.dat")
            setattr(self,"By",By)
            setattr(self,"Bz",Bz) #This attribute can be called as mydata.Bz or even "Bz" !
            setattr(self,"vy",vy)
            setattr(self,"vz",vz)
            setattr(self,"By_label","$B_y$")
            setattr(self,"Bz_label","$B_z$")
            setattr(self,"vy_label","$v_y$")
            setattr(self,"vz_label","$v_z$")

        T  = np.loadtxt(filename+"/temperature.dat")
        nh = np.loadtxt(filename+"/nh.dat")


        nr_bins=int(info[0])
        time =info[1]


        print("Reading model at time   =  "+str(time)+ ' kyr')
        if(len(info)>2):
            alpha_cloud=info[2]
            print("alpha_cloud             =  "+str(alpha_cloud))
            setattr(self,"alpha_cloud",alpha_cloud)

        print("Number of zones         =  "+str(nr_bins))
        if(os.path.exists(filename+"/info_dust.dat")):
            infodust=np.loadtxt(filename+"/info_dust.dat")
            ndust=int(infodust[0])
            smin=infodust[1]
            smax=infodust[2]
            scutmin=infodust[3]
            scut=infodust[4]       
            mrn = infodust[5]
        else:
            ndust=0
            smin=0
            smax=0
            scutmin=0
            mrn=0
        if(ndust>0):
            print("Number of dust species  =  "+str(ndust))
            print("min grain size          =  "+str(smin)+' cm')
            print("grain size cut min      =  "+str(scutmin)+' cm')
            print("grain size cut          =  "+str(scut)+' cm')
            print("max grain size          =  "+str(smax)+' cm')
            print("mrn slope               =  "+str(mrn))
        
        
        setattr(self,"time",time)
        setattr(self,"nr_bins",nr_bins)
        if(ndust>0):
            setattr(self,"ndust",ndust)
            setattr(self,"smin",smin)
            setattr(self,"smax",smax)
            setattr(self,"scutmin",scutmin)
            setattr(self,"scut",scut)
            setattr(self,"mrn",mrn)        

        #Grid
        setattr(self,"grid",grid)
        setattr(self,"grid_label","r [au]")

        #Variables
        setattr(self,"rho",rho)
        setattr(self,"rho_label","$\\rho$ [g cm$^{-3}$]")
        setattr(self,"v",v)
        setattr(self,"v_label","$v$ [ cm s$^{-1}$]")
        setattr(self,"nh",nh)
        setattr(self,"nh_label","$n_{H}$ [cm$^{-3}$]")
        setattr(self,"T",T)
        setattr(self,"T_label","T [K]")
        setattr(self,"B",B)
        setattr(self,"B_label","B [G]")
        if(ndust>0):
            self.read_dust(filename=filename,read_drift=read_drift)
        return


    def data_loader(self,nout=1,path=""):
        # Generate directory name from output number
        infile = self.generate_fname(nout,path)
        return infile
    
    def read_dust(self,filename='',read_drift=False):
        print(filename)
        epsilon  = np.loadtxt(filename+"/epsilon.dat")
        rhodust_long  = np.loadtxt(filename+"/rhodust.dat")
        sdust_long    = np.loadtxt(filename+"/sdust.dat")
        mdust_long    = np.loadtxt(filename+"/mdust.dat")
        vdust_long  = np.loadtxt(filename+"/vdust.dat")
        mminus=  np.loadtxt(filename+"/mminus.dat")
        mplus=  np.loadtxt(filename+"/mplus.dat")
        aminus=  np.loadtxt(filename+"/aminus.dat")
        aplus=  np.loadtxt(filename+"/aplus.dat")

        eta_a = np.loadtxt(filename+"/eta_a.dat")
        eta_h = np.loadtxt(filename+"/eta_h.dat")
        eta_o = np.loadtxt(filename+"/eta_o.dat")

        sigma_o=np.loadtxt(filename+"/sigma_o.dat")
        sigma_p=np.loadtxt(filename+"/sigma_p.dat")
        sigma_h=np.loadtxt(filename+"/sigma_h.dat")

        ni = np.loadtxt(filename+"/ni.dat")
        ne = np.loadtxt(filename+"/ne.dat")
        zdlong= np.loadtxt(filename+"/zd.dat")
        zdlong_shaped = np.reshape(zdlong,(self.ndust,self.nr_bins),order = "C").T

        gamma_d = np.loadtxt(filename+"/gamma_d.dat")
        gamma_d_shaped = np.reshape(gamma_d,(self.ndust,self.nr_bins), order = "C").T

        if (os.path.exists(filename+"/By.dat")):
            vy_dust_long = np.loadtxt(filename+"/vy_dust.dat")
            #vy_dust_shaped=np.reshape(vy_dust,(self.ndust,self.nr_bins),order = "C").T
            vz_dust_long = np.loadtxt(filename+"/vz_dust.dat")
            # vz_dust_shaped=np.reshape(vz_dust,(self.ndust,self.nr_bins),order = "C").T
            for idust in range(self.ndust):

                vy_dust=vy_dust_long[self.nr_bins*idust:self.nr_bins*idust+self.nr_bins]#slice that ranges as [1st value:last value]
                vz_dust=vz_dust_long[self.nr_bins*idust:self.nr_bins*idust+self.nr_bins]#slice that ranges as [1st value:last value]

            
                setattr(self,"vy_dust"+str(idust+1),vy_dust)
                setattr(self,"vy_dust"+str(idust+1)+"_label","$v_{y,d,"+str(idust+1)+"}$ [cm s$^{-1}$]")
                setattr(self,"vz_dust"+str(idust+1),vz_dust)
                setattr(self,"vz_dust"+str(idust+1)+"_label","$v_{z,d,"+str(idust+1)+"}$ [cm s$^{-1}$]")

        
        setattr(self,"gamma_d",gamma_d_shaped)
        setattr(self,"gamma_d_label","Dust Hall factor")
        setattr(self,"zd",zdlong_shaped)
        setattr(self,"zd_label","Dust charge")
        setattr(self,"eta_a",eta_a)
        setattr(self,"eta_a_label","Ambipolar resistivity")        
        setattr(self,"eta_h",eta_h)
        setattr(self,"eta_h_label","Hall resistivity (absolute value)")
        setattr(self,"eta_h_plus",np.where(eta_h>0,eta_h,np.NaN))
        setattr(self,"eta_h_plus_label","Hall resistivity ")
        setattr(self,"eta_h_minus",np.where(eta_h<=0,-eta_h,np.NaN))
        setattr(self,"eta_h_minus_label","Hall resistivity ")   
        setattr(self,"eta_o",eta_o)
        setattr(self,"eta_o_label","Ohmic resistivity")
        setattr(self,"sigma_o",sigma_o)
        setattr(self,"sigma_o_label","Ohmic conductivity [s]")
        setattr(self,"sigma_p",sigma_p)
        setattr(self,"sigma_p_label","Parallel conductivity [s]")
        setattr(self,"sigma_h",sigma_h)
        setattr(self,"sigma_h_label","Hall conductivity [s]")
        setattr(self,"ni",ni)
        setattr(self,"ni_label","Ions density [cm$^{-3}$]")        
        setattr(self,"ne",ne)
        setattr(self,"ne_label","Electrons density [cm$^{-3}$]")
        #setattr(self,"vy_dust",vy_dust_shaped)
        #setattr(self,"vy_dust_label","$v_{y,dust}$ [$cm/s$]")
        #setattr(self,"vz_dust",vz_dust_shaped)
        #setattr(self,"vz_dust_label","$v_{z,dust}$ [$cm/s$]")

        if(read_drift):
            vdrifts= np.loadtxt(filename+"/dust_drifts.dat")
            vdrift_turb=np.reshape(vdrifts[:,0],(self.nr_bins,self.ndust,self.ndust), order='C')
            vdrift_hydro=np.reshape(vdrifts[:,1],(self.nr_bins,self.ndust,self.ndust), order='C')
            vdrift_ad=np.reshape(vdrifts[:,2],(self.nr_bins,self.ndust,self.ndust), order='C')
            vdrift_brow=np.reshape(vdrifts[:,3],(self.nr_bins,self.ndust,self.ndust), order='C')
            setattr(self,"vdrift_turb",vdrift_turb)
            setattr(self,"vdrift_turb_label","Turbulent drift")
            setattr(self,"vdrift_hydro",vdrift_hydro)
            setattr(self,"vdrift_hydro_label","Hydro drift")
            setattr(self,"vdrift_ad",vdrift_ad)
            setattr(self,"vdrift_ad_label","Ambipolar drift")
            setattr(self,"vdrift_brow",vdrift_brow)
            setattr(self,"vdrift_brow_label","Brownian drift")

        setattr(self,"epsilon",epsilon)
        setattr(self,"epsilon_label","Dust-to-Gas Ratio")


        epsdust_glob=np.zeros((self.nr_bins,self.ndust))
        sdust_glob=np.zeros((self.nr_bins,self.ndust))
        mdust_glob=np.zeros((self.nr_bins,self.ndust))
        total_dust2gas=np.zeros(self.nr_bins)
        for idust in range(self.ndust):
            rhodust= rhodust_long[self.nr_bins*idust:self.nr_bins*idust+self.nr_bins] #Not needed if previously reshaped
            sdust  = sdust_long[self.nr_bins*idust:self.nr_bins*idust+self.nr_bins]
            mdust  = mdust_long[self.nr_bins*idust:self.nr_bins*idust+self.nr_bins]
            epsilondust= rhodust_long[self.nr_bins*idust:self.nr_bins*idust+self.nr_bins]/self.rho
            epsdust_glob[:,idust]=epsilondust
            total_dust2gas+=epsdust_glob[:,idust]
            sdust_glob[:,idust]=sdust
            mdust_glob[:,idust]=mdust
            vdust=vdust_long[self.nr_bins*idust:self.nr_bins*idust+self.nr_bins]#slice that ranges as [1st value:last value]
            
            zdust=abs(zdlong[self.nr_bins*idust:self.nr_bins*idust+self.nr_bins])
            setattr(self,"rhod"+str(idust+1),rhodust)
            setattr(self,"rhod"+str(idust+1)+"_label","$\\rho_{d,"+str(idust+1)+"}$ [g cm$^{-3}$]")
            setattr(self,"vd"+str(idust+1),vdust)
            setattr(self,"vd"+str(idust+1)+"_label","$v_{d,"+str(idust+1)+"}$ [cm s$^{-1}$]")
            setattr(self,"sd"+str(idust+1),sdust)
            setattr(self,"sd"+str(idust+1)+"_label","$s_{"+str(idust+1)+"}$ [cm]")
            setattr(self,"md"+str(idust+1),mdust)
            setattr(self,"md"+str(idust+1)+"_label","$m_{"+str(idust+1)+"}$ [g]")
            setattr(self,"epsilon"+str(idust+1),epsilondust)
            setattr(self,"epsilon"+str(idust+1)+"_label","Dust-to-Gas Ratio bin"+str(idust+1))
            setattr(self,"dust_ratio"+str(idust+1),epsilondust/(1.+total_dust2gas))
            setattr(self,"dust_ratio"+str(idust+1)+"_label","Dust Ratio bin"+str(idust+1))
            setattr(self,"epsilon_tot",total_dust2gas)
            setattr(self,"epsilon_tot_label","Dust-to-Gas Ratio")
        setattr(self,"epsilon_all",epsdust_glob)
        setattr(self,"sdust_all",sdust_glob)
        setattr(self,"sdust_label","s [cm]")
        setattr(self,"mdust_all",sdust_glob)
        setattr(self,"mminus",mminus)
        setattr(self,"mplus",mplus)
        setattr(self,"aminus",aminus)
        setattr(self,"aplus",aplus)
        return 
        
    def generate_fname(self,nout,path="",ftype="",cpuid=1,ext=""): #Take from OSIRIS (Vaytet)
        
        if len(path) > 0:
            if path[-1] != "/":
                path=path+"/"
        
        if nout == -1:
            filelist = sorted(glob.glob(path+"output*"))
            number = filelist[-1].split("_")[-1]
        else:
            number = str(nout).zfill(5)

        infile = path+"output_"+number
        if len(ftype) > 0:
            infile += "/"+ftype+"_"+number
            if cpuid >= 0:
                infile += ".out"+str(cpuid).zfill(5)
        
        if len(ext) > 0:
            infile += ext
            
        return infile
        

    def get_size_peak(self,ir=0):

         epsdust=self.epsilon_all
         sdust=self.sdust_all
         distrib=epsdust[ir]
         sdust_loc=sdust[ir]

         i_max=np.argwhere(distrib==np.amax(distrib)) #Python stores the index in a list
         size_peak=sdust_loc[i_max[0]] #same here apparently
         distrib_peak=distrib[i_max[0]]

         setattr(self,"size_peak",size_peak[0])

         return size_peak[0]
     
    def get_massdistrib_peak(self,ir=0):

         epsdust=self.epsilon_all
         sdust=self.sdust_all
         distrib=epsdust[ir]
         sdust_loc=sdust[ir]

         i_max=np.argwhere(distrib==np.amax(distrib)) #Python stores the index in a list
         distrib_peak=distrib[i_max[0]]

         return distrib_peak[0]

         
    def compute_opacities(self,model=''):
        def SumFunc_Generic(delta,sigma,sigma_eff):
            n=len(delta)
            return np.sum(delta*(sigma-sigma_eff)/(sigma+((n-1)*sigma_eff)))
        def makeProblem(delta_array,sigma_array):
            f=lambda sigma_e : SumFunc_Generic(delta_array,sigma_array,sigma_e)
            return f
        dict_nrm=fop.dic_comp('NRM')
        cons='new'
        data=np.loadtxt('/Users/ul264359/Documents/libraries/dune/'+cons+"_cons/Normal_silicates/n_olivine.dat")
        n_olivine=data[:,1]+1j*data[:,2]
        data=np.loadtxt('/Users/ul264359/Documents/libraries/dune/'+cons+"_cons/Normal_silicates/n_orthopyroxene.dat")
        n_pyroxene=data[:,1]+1j*data[:,2]
        data =np.loadtxt('/Users/ul264359/Documents/libraries/dune/'+cons+"_cons/Normal_silicates/n_iron.dat")
        n_iron =data[:,1]+1j*data[:,2]
        data =np.loadtxt('/Users/ul264359/Documents/libraries/dune/'+cons+"_cons/Normal_silicates/n_troilite.dat")
        n_troilite =data[:,1]+1j*data[:,2]
        data =np.loadtxt('/Users/ul264359/Documents/libraries/dune/'+cons+"_cons/Normal_silicates/n_volatile_organics.dat")
        n_organics =data[:,1]+1j*data[:,2]
        data =np.loadtxt('/Users/ul264359/Documents/libraries/dune/'+cons+"_cons/Normal_silicates/n_water_ice.dat")
        n_Water_ice=data[:,1]+1j*data[:,2]
        lambda_k= data[:,0]*1e-4
        nu=(3e10)/lambda_k
        
        dict_nrm=fop.dic_comp('NRM')
        frac_olivine=dict_nrm["frac_olivine"]
        frac_iron=dict_nrm["frac_iron"]
        frac_pyroxene=dict_nrm["frac_pyroxene"]
        frac_organics=dict_nrm["frac_organics"]
        frac_Troilite=dict_nrm["frac_Troilite"]
        frac_Water_ice=dict_nrm["frac_Water_ice"]
        
        rho_olivine=dict_nrm["dens_olivine"]
        rho_iron=dict_nrm["dens_iron"]
        rho_pyroxene=dict_nrm["dens_pyroxene"]
        rho_organics=dict_nrm["dens_organics"]
        rho_Troilite=dict_nrm["dens_Troilite"]
        rho_Water_ice=dict_nrm["dens_Water_ice"]
        
        
        
        Frac_array=np.asarray([frac_olivine,frac_iron,frac_pyroxene,frac_organics,frac_Troilite,frac_Water_ice])
        norm=np.sum(Frac_array)
        Frac_array/=norm
        sigma_array=np.asarray([n_olivine,n_iron,n_pyroxene,n_organics,n_troilite,n_Water_ice])  
        rho_grain = (rho_olivine*frac_olivine+ rho_iron*frac_iron + rho_pyroxene*frac_pyroxene+rho_Troilite*frac_Troilite+rho_organics*frac_organics+rho_Water_ice*frac_Water_ice)/norm

        Frac_array=np.asarray([frac_olivine,frac_iron,frac_pyroxene,frac_organics,frac_Water_ice])
        norm=np.sum(Frac_array)
        Frac_array/=norm
        sigma_array=np.asarray([n_olivine,n_iron,n_pyroxene,n_organics,n_Water_ice])  
        rho_grain = (rho_olivine*frac_olivine+ rho_iron*frac_iron + rho_pyroxene*frac_pyroxene+rho_organics*frac_organics+rho_Water_ice*frac_Water_ice)/norm
        
        n_mixture=np.zeros(len(n_olivine),dtype=complex)
        for i in range(len(n_mixture)):
            f=makeProblem(Frac_array,sigma_array[:,i])
            mixture=mpm.findroot(f,x0=1.0+1.0j)
            #print(mixture)
            n_mixture[i]=mixture
        if(model=='astrosil'):
            data=np.loadtxt('/Users/ul264359/Documents/libraries/dune/'+cons+"_cons/astro_silicates.dat")
            lambda_k= data[:,0]*1e-4
            n_mixture=data[:,3]+1j*data[:,4]
            data=np.loadtxt('/Users/ul264359/Documents/libraries/dune/'+cons+"_cons/astro_silicates2.dat")
            lambda_k= data[:,0]*1e-4
            n_mixture=data[:,1]+1j*data[:,2]
            nu=(3e10)/lambda_k
            rho_grain=2.3

        ee1 = fop.e1(np.real(n_mixture),np.imag(n_mixture))
        ee2 = fop.e2(np.real(n_mixture),np.imag(n_mixture))
        sigma= fop.sigmajk(lambda_k,ee1,ee2,0.333333333)
        Kappadust=np.zeros((self.nr_bins,len(lambda_k)))
        for icell in range(self.nr_bins):
            for idust in range(self.ndust):
                Kappadust[icell,:]+=self.rho[icell]*fop.Kappaj(self.epsilon_all[icell,idust],rho_grain,fop.Hj(fop.xj(lambda_k,self.sdust_all[icell,idust],self.sdust_all[icell,idust]),ee1,ee2),sigma,sigma,sigma)        
        setattr(self,"lambda_k",lambda_k)
        setattr(self,"n_freq",len(lambda_k))        
        setattr(self,"opacity",Kappadust)        
        return Kappadust

    def get_optical_depth(self):
        opacity=self.opacity
        grid=self.grid*1.5e13
        dgrid=grid-np.roll(grid,1)
        dgrid[0]=0.5*grid[0]
        tau=np.zeros((self.nr_bins,self.n_freq))
        for inu in range(self.n_freq):
            tau[:,inu]=np.cumsum(dgrid*opacity[:,inu])        
        setattr(self,"tau",tau)
        return tau

    def get_extinction(self):
        Anu=1.086*self.tau
        lambda_k=self.lambda_k
        dlambda=abs(lambda_k*1e7-540)
        
        i_V=np.argwhere(dlambda==np.amin(dlambda))

        dlambda=abs(lambda_k*1e7-430)
        i_B=np.argwhere(dlambda==np.amin(dlambda))

        extinction=np.zeros((self.nr_bins,self.n_freq))
        for icell in range(self.nr_bins):
            AnuV=Anu[:,i_V[0][0]]
            AnuB=Anu[:,i_B[0][0]]
            #extinction[icell,:]=abs(Anu[icell,:]-AnuV[icell])/abs(AnuB[icell]-AnuV[icell])
            extinction[icell,:]=abs(Anu[icell,:])/abs(AnuV[icell])

        setattr(self,"extinction",np.sum(extinction,axis=0))        
         
