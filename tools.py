import matplotlib.pyplot as plt
import numpy as np
import glob
import dune
import consts
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator

def collapse_designor1(Mcloud,alpha):
    ct=consts.constants()
    G=ct["g"]
    kb=ct["kb"]
    c=ct["c"]
    mh=ct["mh"]
    msun=ct["msun"]
    pc=ct["pc"]
    au=ct["au"]
    mu_gas=2.31
    mc=Mcloud*msun
    T=10.0
    Rcloud=alpha*2./5.*G*mc*mu_gas*mh/kb/T
    return Rcloud/au, mc/(4./3.*np.pi*Rcloud**3.)

    
