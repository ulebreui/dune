import matplotlib.pyplot as plt
import numpy as np
import glob
import dune
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator


def constants():
    Gravity=6.674e-8
    k_boltz=1.38e-16
    c_light=2.997e10
    e_el=4.8e-10
    mH= 1.67e-24
    me = 9.109e-28
    AU= 1.496e13
    year = 31556926
    kyear = year*1000
    msun= 2e33
    pc=3.08e18
    mu_gas=2.31
    gamma = 1.66667
    
    dict_const = {}
    
    dict_const["g"]=Gravity
    dict_const["kb"]=k_boltz
    dict_const["c"]=c_light
    dict_const["e"]= e_el
    dict_const["mh"]= mH
    dict_const["me"]=me
    dict_const["au"]=AU
    dict_const["year"]=year
    dict_const["msun"]=msun
    dict_const["pc"]=pc
    dict_const["mu_gas"]=mu_gas
    dict_const["kyear"]=kyear
    dict_const["gamma"]=gamma

    return dict_const
