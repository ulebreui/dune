import numpy as np
import matplotlib.pyplot as plt

mu_gas=2.31
mH=1.67e-24
rhograin=2.3
epsilon=0.01
smax_ini=2.5e-5
def cst(nh,T):
    return 1.72*1e2*(nh/1e4)**(-1./4.)*(T/10.0)**(-1./4.)*(1e6)**(1./2.)*epsilon*nh*mu_gas*mH/(4./3.*rhograin)

def solution(Cst,t):
    return np.sqrt(8./(3.*np.pi))*(0.5*Cst*t+np.sqrt(smax_ini))**2.

t_conv= 365.24*3600.*24*1000.
t=np.linspace(0.,30.,202)*t_conv
Sol=solution(cst(1e9,10.12),t)

print(Sol)
plt.plot(t/t_conv,Sol)
plt.show()
